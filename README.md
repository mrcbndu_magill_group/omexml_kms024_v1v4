# OMEXML_kms024_v1v4

OME-XML metadata of kms024_v1v4 images including ROIs. 

http://chamber.mrc.ox.ac.uk/webclient/?show=dataset-51



## What are included?

This repository contains three folders:

+ `ome` ... contains `imageID.ome.xml` files

+ `ome_indented` ... contains `imageID.ome_indexed.xml` files, in which XML is indented for readability.

+ `ome_indented-v` .. contains `imageID.ome_indexed-v.xml` files, in which version numbers of objects are replaced with `?`  in the indented XML in order to highlight actual changes made in text diff comparison.

where `imageID` is the image ID numbers defined in OMERO. 



The Microsoft Excel file

`kms024_v1v4_AnnotationsList.xlsx`

contains the list of structure abbreviations used in the dataset with image IDs and ROI IDs.

In order to access a specific image, use an image ID (eg. 127) as in:

http://chamber.mrc.ox.ac.uk/webclient/img_detail/127

In the ROIs tab of OMERO.iviewer, ROI objects are shown in the order of ROI IDs and by hovering a mouse cursor data tooltip will show the ROI ID of that ROI in the list.



## How to apply OME-XML to a CHAMBER image

In order to apply an OME-XML to CHAMBER image data:

1. Download a full resolution image as OME-TIFF from CHAMBER website. https://help.openmicroscopy.org/export.html#download 
2. Use [the Bio-Formats Command Line Tools](https://docs.openmicroscopy.org/latest/bio-formats/users/comlinetools/index.html) to inject the specified OME-XML (a `.ome.xml` file in the `ome ` folder) to the corresponding image file (OME-TIFF) as in `tiffcomment -set 'newmetadata.ome.xml' image1.ome.tif`.  https://docs.openmicroscopy.org/latest/bio-formats/users/comlinetools/edit.html
3. Then open the saved OME-TIFF with Fiji. Bio-Formats Importer allows you to open an OME-TIFF file with ROIs.

## See also

* Bio-Formats: https://www.openmicroscopy.org/bio-formats/
* The OME-XML format: https://docs.openmicroscopy.org/latest/ome-model/ome-xml/
* Fiji: https://fiji.sc/
* OMERO: https://www.openmicroscopy.org/omero/





## Contacts

Dr Kouichi. C. Nakamura, kouichi.nakamura@pharm.ox.ac.uk

Mr Ben Micklem, ben.micklem@pharm.ox.ac.uk

Prof Peter J. Magill, peter.magill@pharm.ox.ac.uk

MRC Brain Network Dynamics Unit, Department of Pharmacology, University of Oxford,

Mansfield Road, Oxford OX1 3TH.















